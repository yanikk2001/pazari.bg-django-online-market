from .models import User, Listing

class showlisting:
    def __init__(self, entry, bool):
        self.listing = entry
        self.is_favourite = bool

def getListings(request, listings):
        entries = []
        for entry in listings:
                bool = entry.favourite.filter(id=request.user.id).exists()
                listing = showlisting(entry, bool)
                listing.listing
                entries.append(listing)
        return entries

def createNewListing(request, form):
        label = form.cleaned_data['label']
        price = form.cleaned_data['price']
        description = form.cleaned_data['description']
        tag = form.cleaned_data['tag']
        image = form.cleaned_data['image']
        image1 = form.cleaned_data['image1']
        image2 = form.cleaned_data['image2']
        image3 = form.cleaned_data['image3']
        user = request.user
        instance = Listing(label = label, price = price, description = description, owner = user)
        instance.save() 
        instance.tag.set(tag)
        instance.image = image
        instance.image1 = image1
        instance.image2 = image2
        instance.image3 = image3
        instance.save() #Saving twice because the id needed for the filepath of the image is generated after saving

        return instance.pk