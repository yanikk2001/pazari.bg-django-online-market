from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("register", views.register, name="register"),
    path("new-listing", views.addListing, name="new-listing"),
    path("listing-page/<int:id>", views.listingPage, name="listing-page"),
    path("wishlist/<str:username>", views.wishlistPage, name="wishlist"),
    path("favourite/<int:id>/<str:parentId>/<int:pos>/<str:loc>", views.favouriteListing, name="favourite-listing"),
    path("profile-page/<str:username>", views.profilePage, name="profile-page"),
    path("delete-listing/<int:id>", views.deleteListing, name="delete-listing"),
] 

