# Generated by Django 3.2.6 on 2021-09-09 11:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auctions', '0004_auto_20210909_1137'),
    ]

    operations = [
        migrations.AddField(
            model_name='listing',
            name='tags',
            field=models.CharField(choices=[('TECHNOLOGY', 'Technology'), ('ANIMALS', 'Animals'), ('CARS', 'Automobiles')], default='CARS', max_length=30),
        ),
    ]
