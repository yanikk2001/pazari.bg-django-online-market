from commerce.settings import MEDIA_ROOT
from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.db.models import Q
from django.contrib import messages

from .models import User, Listing
from .forms import *
from .views_helperfunctions import *
      
def index(request):
    all_entries = Listing.objects.all().filter(is_for_sale = True)
    entries = getListings(request, all_entries)

    return render(request, "auctions/index.html", {
        "entries": entries
    })
    
def login_view(request):
    if request.method == "POST":

        # Attempt to sign user in
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)

        # Check if authentication successful
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse("index"))
        else:
            return render(request, "auctions/login.html", {
                "message": "Invalid username and/or password."
            })
    else:
        return render(request, "auctions/login.html")


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse("index"))


def register(request):
    if request.method == "POST":
        username = request.POST["username"]
        email = request.POST["email"]

        #Ensure password matches confirmation
        password = request.POST["password"]
        confirmation = request.POST["confirmation"]
        if password != confirmation:
            return render(request, "auctions/register.html", {
                "message": "Passwords must match", "username": username, "email": email
            })

        # Attempt to create new user
        try:
            user = User.objects.create_user(username, email, password)
            user.save()
        except ValueError as e:
            return render(request, "auctions/register.html", {
                "message": str(e), "username": username, "email": email
            })
        except IntegrityError as e:
            exception = str(e)
            if exception == "UNIQUE constraint failed: auctions_user.email":
                message = "Email already taken"
            else:
                message = "Username already taken"

            return render(request, "auctions/register.html", {
                "message": message, "username": username, "email": email
            })
        login(request, user)
        return HttpResponseRedirect(reverse("index"))
    else:
        return render(request, "auctions/register.html")

def addListing(request):
    if request.method == "POST":
        form = ListingForm(request.POST, request.FILES)
        if form.is_valid():
            pk = createNewListing(request, form)
            return HttpResponseRedirect(f"listing-page/{pk}")
        else:
            price = int(request.POST['price'])
            message = "Price is out of range!" if price < 0 or price > 9999999.99 else None
            return render(request, "auctions/new-listing.html", {
            "form": form, "message": message
            })
    else:
        form = ListingForm()   
        return render(request, "auctions/new-listing.html", {
            "form": form
        })

def listingPage(request, id):
    listing = Listing.objects.get(pk = id)
    more_listings = listing.owner.listings.all().filter(~Q(pk = listing.pk), is_for_sale = True)[:5] #gets more listings by the same user which are active and with different primary key, returns the first 5results
    user_listigs = getListings(request, more_listings)
    if request.method == "POST":
        commentForm = CommentForm(request.POST)
        if commentForm.is_valid():
            instance = Comment(publisher = request.user, listing = listing, body = commentForm.cleaned_data['body'])
            instance.save()
    
    commentForm = CommentForm()

    comments = listing.comments.all().order_by('-date_created')
    
    return render(request, "auctions/listing-page.html", {
        "listing": listing, "comments": comments, "user_listings": user_listigs, "commentForm": commentForm
    })

def wishlistPage(request, username):
    userwishlist = User.objects.get(username = username).favourite.all()
    wishlist = getListings(request, userwishlist)

    return render(request, "auctions/wishlist.html", {
        "wishlist": wishlist
    })

def favouriteListing(request, id, parentId, pos, loc):
    if not request.user.is_authenticated:
        messages.info(request, 'You have to be logged in to add listings to your wishlist')
        if loc == 'index':
            return HttpResponseRedirect(reverse('index'))
        if loc == 'listingPage':
            return HttpResponseRedirect(reverse('listing-page', args=[parentId]))
        if loc == 'profilePage':
            return HttpResponseRedirect(reverse('profile-page', args=[parentId]))
        
    if request.method == "POST":
        listing = Listing.objects.get(pk = id)
        if listing.favourite.filter(id = request.user.id).exists():
            listing.favourite.remove(request.user)
        else:
            listing.favourite.add(request.user)
        
        if loc == 'index':
            return HttpResponseRedirect(f"/#{pos}")
        if loc == 'listingPage':
            return HttpResponseRedirect(reverse('listing-page', args=[parentId]))
        if loc == 'wishlist':
            return HttpResponseRedirect(reverse('wishlist', args=[parentId]))
        if loc == 'profilePage':
            return HttpResponseRedirect(reverse('profile-page', args=[parentId]))
        
def profilePage(request, username):
    user = User.objects.get(username = username)
    listings = user.listings.all()
    user_listings = getListings(request, listings)

    if request.method == 'POST':
        form = UserProfilePicForm(request.POST ,request.FILES)
        if form.is_valid():
            image = form.cleaned_data['image']
            user.image = image
            user.save()
        else:
            return HttpResponseRedirect(reverse ('index'))

    form = UserProfilePicForm()
    return render(request, "auctions/user-profile.html", {
        "user": user, "user_listings": user_listings, "form": form
    })

def deleteListing(request, id):
    listing = Listing.objects.get(pk = id)
    listing.delete()
    return HttpResponseRedirect(reverse('index'))
        