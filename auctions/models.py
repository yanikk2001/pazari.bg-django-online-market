from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class MyAccountManager(BaseUserManager):

    def create_user(self, username, email, password=None):
        if not email:
            raise ValueError("Users must have an email address")
        if not username:
            raise ValueError("Users must have an username")
        if not password:
            raise ValueError("Enter password")
        user = self.model(
            email = self.normalize_email(email),
            username = username,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(
            email=self.normalize_email(email),
            username = username,
            password=password,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

def get_user_profile_image(self, filepath):
    return f'user_profiles/{self.id}/user_image.png'
def get_user_default_profile_image():
    return 'def_user_profile_img/def_img.png'

class User(AbstractBaseUser):
    email = models.EmailField(verbose_name="email", max_length=60, unique=True)
    username = models.CharField(max_length=30, unique=True)
    date_joined = models.DateTimeField(verbose_name="date joined", auto_now_add=True)
    last_login = models.DateTimeField(verbose_name="last login", auto_now=True, null=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    hide_email = models.BooleanField(default=True)
    image = models.ImageField(max_length = 255, upload_to=get_user_profile_image, null= True, blank=True, default=get_user_default_profile_image)

    objects = MyAccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

def get_profile_listing_image_filepath(self, filepath):
        return f'listings_media/{self.id}/profile_image.png'
def get_listing_image_filepath(self, filepath):
        return f'listings_media/{self.id}/listing_image.png'

def get_default_profile_listing_image():
    return "def_profile_listing_img/def_profile.png"

class Tag(models.Model):
    tag_name=models.CharField(max_length=30, unique=True)

    def __str__(self):
        return f"{self.tag_name}"

class Listing(models.Model):
    label = models.CharField(max_length=30)
    price = models.FloatField(validators=[MinValueValidator(0.0), MaxValueValidator(9999999.99)])
    description = models.TextField(max_length=300, null=True)
    tag = models.ManyToManyField(Tag)
    image = models.ImageField(max_length = 255, upload_to=get_profile_listing_image_filepath, null= True, blank=True, default=get_default_profile_listing_image)
    image1 = models.ImageField(max_length = 255, upload_to=get_listing_image_filepath, null= True, blank=True, default=get_default_profile_listing_image)
    image2 = models.ImageField(max_length = 255, upload_to=get_listing_image_filepath, null= True, blank=True, default=get_default_profile_listing_image)
    image3 = models.ImageField(max_length = 255, upload_to=get_listing_image_filepath, null= True, blank=True, default=get_default_profile_listing_image)
    date_created = models.DateField(auto_now_add=True)
    is_for_sale = models.BooleanField(default=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="listings", null=True)
    favourite = models.ManyToManyField(User, related_name= 'favourite', blank=True)

    def __str__(self):
        return f"{self.label} {self.price}"

class Comment(models.Model):
    publisher = models.ForeignKey(User, on_delete=models.CASCADE, related_name="comments")
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE, related_name="comments")
    date_created = models.DateTimeField(auto_now_add=True)
    body = models.CharField(max_length=150)

    class Meta:
        ordering = ['date_created']

    def __str__(self):
        return f"id:{self.pk} publisher: {self.publisher} date: {self.date_created}"  


