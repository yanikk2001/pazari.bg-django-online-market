from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from auctions.models import Comment, User, Listing, Tag

class AccountAdmin(UserAdmin):
    list_display = ('email', 'username', 'date_joined', 'last_login', 'is_admin', 'is_staff')
    search_fields = ('email', 'username')
    readonly_fields = ('id', 'date_joined', 'last_login')

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(User,AccountAdmin)

admin.site.register(Tag)

class CommentAdmin(admin.ModelAdmin):
    list_display = ('id', 'publisher', 'listing', 'date_created')
    list_filter = ('date_created', 'listing')
    search_fields = ('id', 'publisher', 'body', 'listing')
    actions = ['approve_comments']

    def approve_comments(self, request, queryset):
        queryset.update(active=True)

admin.site.register(Comment, CommentAdmin)

class ListingAdmin(admin.ModelAdmin):
    list_display = ("label", "price", "is_for_sale", "owner", "date_created")
    list_filter = ("is_for_sale", "date_created")
    search_fields = ("label", "owner", "date_created")
    readonly_fields = ("id", "date_created")

    filter_horizontal = ('tag',)

admin.site.register(Listing,ListingAdmin)


