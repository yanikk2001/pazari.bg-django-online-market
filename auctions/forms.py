from django.db.models import fields
from django.forms import ModelForm
from django import forms
from .models import *

class ListingForm(ModelForm):

    class Meta:
        model = Listing
        fields = ['label', 'price', 'description', 'tag', 'image', 'image1', 'image2', 'image3']

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('body',)

class UserProfilePicForm(ModelForm):
    class Meta:
        model = User
        fields = ('image',)