![alt text](./media/assets/logo.png "Pazari.bg")

# Pazari.bg - Django Online Marketplace

'First' ever website I make. Represents an online marketplace like 'ebay'. Build using the 'Django' web framework and 'sqlite'

Comes with the virtual machine

To run open virtual machine, excecute: 
'''
.virtualenvs\djangodev\Scripts\activate.bat
'''

To start local server:
'''
py manage.py runserver
'''